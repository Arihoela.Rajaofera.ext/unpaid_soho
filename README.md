# Unpaid SoHo
AI project edited by Ari RAJAOFERA.

## Description
The main goal is to detect bad customers with inpaid invoices, from SME and SoHo customers from Luminus.

## Donwload the project
`1. Clone the project repository :`
```
 git clone https://gitlab.com/Arihoela.Rajaofera.ext/unpaid_soho.git
```

`2. Install the required dependencies :`
```
 pip install -r requirements.txt
```

## Install the prerequisited tools

- [ ] [Set up project tools](https://luminus.atlassian.net/wiki/spaces/NEODOC/pages/850002020/Development+tools+setup.)


## Run & Deploy with Conveyor

```
conveyor project generate-config --name <your_project>
conveyor project run
conveyor build
conveyor deploy --env <your_env> --wait
```

