FROM public.ecr.aws/dataminded/spark-k8s-glue:v3.2.1-2.13-hadoop-3.3.1-v6

ENV PYSPARK_PYTHON python3
WORKDIR /opt/spark/work-dir
USER 0

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ARG spark_uid=185
USER ${spark_uid}
