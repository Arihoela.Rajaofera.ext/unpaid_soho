import numpy as np
from sklearn.preprocessing import LabelEncoder
import pandas as pd

def preprocess_input_data(df, string_date):
    
    # Deleting alphabetical part of the postal code 
    df['bill_address_post_code'] = df['bill_address_post_code'].str.extract('(\d+)') 
    
    cols_list = ['finl_str_scr', 'lcl_acty_cd',
                'cainvitem_amount_eur', 'party_vat', 'bill_address_post_code']
    for col in cols_list:
        df[col] = pd.to_numeric(df[col])
    
    df.columns = df.columns.str.upper()
    df = df[['PARTY_VAT','FINL_STR_SCR','RAT','LCL_ACTY_CD','CAINVITEM_AMOUNT_EUR','CAINVITEM_DUE_DT'
             ,'CAINVITEM_CLEARING_DT','CAINVITEM_DOC_TYPE_CODE','BILL_ADDRESS_POST_CODE']]

    cols_list = ['FINL_STR_SCR', 'LCL_ACTY_CD']
    df[cols_list] =  df[cols_list].fillna(0.0)
    df['RAT'].fillna('O-', inplace = True)
    df['CAINVITEM_CLEARING_DT'].fillna(string_date, inplace=True)
    df['BILL_ADDRESS_POST_CODE'].fillna(0, inplace = True) # unknown adress
    
    df=df.reset_index(drop=True)
    
    # Creating variables to predict
    df['DATEDIFF'] = (df['CAINVITEM_CLEARING_DT'] - df['CAINVITEM_DUE_DT']).dt.days
    df['VARIABLE_PREDICT_1'] = np.where(df['DATEDIFF'] > 15, 1, 0)
    df['VARIABLE_PREDICT_2'] = np.where(df['DATEDIFF'] > 30, 1, 0)
    
    # Normalizing amounts
    df_FJ = df.loc[df.CAINVITEM_DOC_TYPE_CODE == "FJ"]
    df_FV = df.loc[df.CAINVITEM_DOC_TYPE_CODE == "FV"]
    mean_amount_FJ = df_FJ.groupby("PARTY_VAT").mean()["CAINVITEM_AMOUNT_EUR"]
    mean_amount_FV = df_FV.groupby("PARTY_VAT").mean()["CAINVITEM_AMOUNT_EUR"]
    df=df.merge(mean_amount_FJ.rename('MEAN_FJ'),left_on='PARTY_VAT', right_index=True)
    df=df.merge(mean_amount_FV.rename('MEAN_FV'),left_on='PARTY_VAT', right_index=True)
    df=df[((df.CAINVITEM_AMOUNT_EUR < df.MEAN_FJ*3)&(df.CAINVITEM_AMOUNT_EUR > df.MEAN_FJ*0.5) & (df.CAINVITEM_DOC_TYPE_CODE == "FJ")) | ((df.CAINVITEM_AMOUNT_EUR < df.MEAN_FV*3)&(df.CAINVITEM_AMOUNT_EUR > df.MEAN_FV*0.5) & (df.CAINVITEM_DOC_TYPE_CODE == "FV"))]
    df = df.drop(["MEAN_FJ","MEAN_FV"], axis=1)
    
    # Special encoding for RAT score
    """
        Here I made a ranking of the RAT score.
        The company, with the most paid invoices, is the first in the ranking.
        In contrary, the companies with the least unpaid invoices, are the last in the ranking.
    """
    rat_names = df['RAT'].loc[df.VARIABLE_PREDICT_1 == 1].value_counts() # stores RAT names in case they need to be retrieved
    for index, val in enumerate(rat_names.items()):
        if (df['RAT'] == val[0]).any():
            df.loc[df['RAT'] == val[0] , 'RAT'] = index+1
        else : 
            df.loc[df['RAT'] == val[0] , 'RAT'] = 111
    rat_list = list(df['RAT'].unique())
    rest_rat_list = [elem for elem in rat_list if isinstance(elem, str)]
    for rat in rest_rat_list :
        df.loc[df['RAT'] == rat , 'RAT'] = 0
    
    return df

def encode_columns(X):
    X_encoded = X.copy()
    label_encoders = {} # stock the label encoder for each column
    for column in {
        'PARTY_VAT','LCL_ACTY_CD', 'CAINVITEM_DOC_TYPE_CODE'
    } :
        encoder_column = LabelEncoder()
        X_encoded[column] = encoder_column.fit_transform(X_encoded[column])
        label_encoders[column] = encoder_column
        
    return X_encoded, label_encoders

def decode_columns(X_test, label_encoders):
    encoded_columns = ['PARTY_VAT', 'LCL_ACTY_CD', 'CAINVITEM_DOC_TYPE_CODE']
    for column in encoded_columns:
        inverse_values = label_encoders[column].inverse_transform(X_test[column])
        X_test[column] = inverse_values
        
    return X_test
