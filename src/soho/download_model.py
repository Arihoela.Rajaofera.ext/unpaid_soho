import boto3
import os
import joblib

#bucket_name = "neo-s3-datalake-dev-ogadwv"
bucket_name = "neo-s3-datalake-pro-udaijd"

# Download models from S3 to local folder
def download_files_in_folder(bucket_name, s3_folder_path, local_destination):
    s3 = boto3.client('s3')
    bucket = boto3.resource('s3').Bucket(bucket_name)

    for obj in bucket.objects.filter(Prefix=s3_folder_path):
        if obj.key != s3_folder_path:
            local_file_path = os.path.join(local_destination, os.path.basename(obj.key))
            os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
            if not os.path.exists(local_file_path):
                s3.download_file(bucket_name, obj.key, local_file_path)
                
def load_models(model_names):
    dir_name = os.path.dirname(__file__)
    local_destination = f"{dir_name}/../../models"
    download_files_in_folder(bucket_name, "dataproduct/sohocredit/unpaidsoho/models", local_destination)
    
    models = []
    
    for name in model_names :
        with open(f"{dir_name}/../../models/{name}.pkl", "rb") as f:
            model = joblib.load(f)
            models.append(model)
    return models

