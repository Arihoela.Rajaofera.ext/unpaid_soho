from pyspark.sql import SparkSession
from pyspark.sql.utils import AnalysisException
from datetime import timedelta, datetime

#bucket_name = "s3://neo-s3-datalake-dev-ogadwv/clean/"
bucket_name = "s3://neo-s3-datalake-pro-udaijd/clean/"

# Read the most recent parquet in the datalake
def read_parquet(spark, path, string_date):
    i = 0
    while True:
        try:
            parquet = spark.read.parquet(path + string_date + "/")
            print("Recovering datas dating from " + str(i) + " day(s).")
            return parquet
        except AnalysisException:
            i +=1 
            date_dt = datetime.strptime(string_date, "%Y-%m-%d") - timedelta(days=1)
            string_date = str(date_dt.date())
    

# Function to recover input datas from AWS S3 and stock them in Pandas dataframe
def get_input_data(date_dt):
    spark = SparkSession.builder.appName('unpaid_SoHo').getOrCreate()
    
    for table, path in {
        "accountDBEram" : "sohocredit/account",
        "invoiceDBDwh" : "dwh/dwh_pl/fact_invoice_document_item",
        "customerDBDwh" : "dwh/dwh_pl/dim_customer"
    }.items() :
        read_parquet(spark, bucket_name + path + "/ingestion_date=", date_dt).createOrReplaceTempView(table)
    
    query = """
            SELECT fidi.cainvitem_amount_eur, 
                fidi.cainvitem_due_dt,
                fidi.cainvitem_clearing_dt,
                fidi.cainvitem_doc_type_code, 
                ea.rat, ea.finl_str_scr,
                ea.lcl_acty_cd, SUBSTR(dc.party_vat,3) as party_vat,
                dc.bill_address_post_code
            FROM invoiceDBDwh fidi
            JOIN customerDBDwh dc
            ON fidi.party_sk=dc.party_sk
            JOIN accountDBEram ea
            ON SUBSTR(dc.party_vat,3) = ea.acct_nbr
            WHERE (fidi.cainvitem_clearing_reason_code IN ('01', '04', '14') or fidi.cainvitem_clearing_reason_code IS NULL) and ea.acct_nbr IS NOT NULL
            limit 10000000;"""
            
    res = spark.sql(query)
        
    return res.toPandas()
