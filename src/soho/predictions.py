from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn import metrics

from preprocessing import encode_columns, decode_columns
from update_input import update_input, concatenate_df
from update_model import update_model


def make_predictions(df, models, model_names, string_date):
    cols_to_drop = ["CAINVITEM_DUE_DT", "CAINVITEM_CLEARING_DT",
                    "DATEDIFF", "VARIABLE_PREDICT_1"]
    df = df.drop(cols_to_drop, axis=1)
    for index, model in enumerate(models):
        
        X = concatenate_df(df, model_names[index], string_date)
        X, label_encoders = encode_columns(X) # encode also the new inputs that we've created before
   
        Y = X[X.columns[-1:]]
        X = X[X.columns[:-1]]
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=53)
        Y_pred = model.predict(X_test)
        
        # Model retraining
        model2 = model.fit(X_train,Y_train.values.ravel())
        Y_pred2 = model2.predict(X_test)
        if metrics.roc_auc_score(Y_test, Y_pred2) > metrics.roc_auc_score(Y_test, Y_pred) :
            model = model2 # take the best model
            Y_pred = Y_pred2
            update_model(model, model_names[index])
        
        # Compute accuracy
        accuracy = model.score(X_test, Y_test)
        print("Accuracy for the "+ model_names[index] + " model : " , accuracy)
        
        # Compute AUC score
        auc = metrics.roc_auc_score(Y_test, Y_pred)
        print("AUC score for the "+ model_names[index] + " model : " , auc)
        
        # Decoding of PARTY_VAT, NACE code, Doc type code
        X_test = decode_columns(X_test, label_encoders)
        
        # Creating new inputs with outputs
        outputs = pd.concat([X_test.reset_index(drop=True), pd.DataFrame((Y_pred > 0).astype(int), columns=[Y_test.columns])], axis=1)
        column_names = ['PARTY_VAT', 'FINL_STR_SCR', 'RAT', 'LCL_ACTY_CD','CAINVITEM_AMOUNT_EUR', 
                        'CAINVITEM_DOC_TYPE_CODE', 'BILL_ADDRESS_POST_CODE','VARIABLE_PREDICT_2']
        outputs.rename(columns=dict(zip(outputs.columns, column_names)), inplace=True)
        update_input(outputs, model_names[index], string_date)
        
            
        # Ranking by attibutes with the highest number of unpaid invoices
        print("")
        print("Rank of PARTY_VAT realising the most of unpaids")
        print(outputs["PARTY_VAT"].loc[outputs.VARIABLE_PREDICT_2 == 1].value_counts())
        
        print("")
        print("Rank of NACE code realising the most of unpaids")
        print(outputs["LCL_ACTY_CD"].loc[outputs.VARIABLE_PREDICT_2 == 1].value_counts())
        
        print("")
        print("Rank of adress postal code realising the most of unpaids")
        print(outputs["BILL_ADDRESS_POST_CODE"].loc[outputs.VARIABLE_PREDICT_2 == 1].value_counts())
            

