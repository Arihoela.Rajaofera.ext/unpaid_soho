import pandas as pd
from get_data import SparkSession, AnalysisException, timedelta, datetime

#bucket_name = "s3://neo-s3-datalake-dev-ogadwv/dataproduct/"
bucket_name = "s3://neo-s3-datalake-pro-udaijd/dataproduct/"

# Function to recover the inputs I have created
def read_my_parquets(spark, path, string_date):
    i = 0
    while True:
        try:
            parquet = spark.read.parquet(path + string_date + "/")
            print("Recovering new datas dating from " + str(i) + " day(s).")
            return parquet
        except AnalysisException:
            i += 1 
            date_dt = datetime.strptime(string_date, "%Y-%m-%d") - timedelta(days=1)
            string_date = str(date_dt.date())
            if i > 7 :
                print("No new datas finded.")
                return None

# Function to concatenate my new inputs with the old inputs
def concatenate_df(df1, model_name, string_date):
    spark = SparkSession.builder.appName('recover_data').getOrCreate()

    parquet = read_my_parquets(spark, bucket_name + "sohocredit/unpaidsoho/outputs/" + model_name + "/ingestion_date=", string_date)
    if parquet is not None : 
        parquet.createOrReplaceTempView("newInputs")
        
        query = """SELECT *
                FROM newInputs ni
                limit 10000000;"""
                
        res = spark.sql(query)
        
        df2 = res.toPandas()        
        df1 = pd.concat([df1, df2], axis=0, ignore_index=True)
        
        df1 = df1.sample(frac=1).reset_index(drop=True)
        
        print("New datas ingestion succeded !")
        
    return df1
    
# Functions storing the inputs in the datalake
def update_input(inputs, model_name, date_dt):
    spark = SparkSession.builder.appName('insert_data').getOrCreate()
    spark_df = spark.createDataFrame(inputs)
    
    spark_df.write.parquet(bucket_name + "sohocredit/unpaidsoho/outputs/" + model_name + "/ingestion_date=" + date_dt + "/")
    
    
    
