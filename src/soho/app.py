import argparse
from datetime import date

from download_model import load_models
from get_data import get_input_data
from preprocessing import preprocess_input_data
from predictions import make_predictions

dt = date.today()
string_date = dt.strftime("%Y-%m-%d")

def main():
    parser = argparse.ArgumentParser(description="unpaid_soho")
    parser.add_argument(
        "-d", "--date", dest="date", help="date in format YYYY-mm-dd", required=True
    )
    parser.add_argument(
        "-e", "--env", dest="env", help="environment we are executing in", required=True
    )
    args = parser.parse_args()
    run(args.env, args.date)


def run(environment: str, date: str):
    
    model_names = ['RegLog_2023_09_05', 'DecTree_2023_09_05', 'RF_2023_09_05'] 
    
    models = load_models(model_names)
    df = get_input_data(string_date)
    df = preprocess_input_data(df, string_date)
    make_predictions(df, models, model_names, string_date)


if __name__ == "__main__":
    main()
