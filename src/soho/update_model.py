from download_model import boto3, os, joblib, bucket_name

def update_model(model, model_name):
    s3 = boto3.client('s3')
    bucket = boto3.resource('s3').Bucket(bucket_name)
    dir_name = os.path.dirname(__file__)
    new_model_path = f"{dir_name}/../../models/{model_name}.pkl"
    joblib.dump(model, new_model_path)
    s3.upload_file(new_model_path, bucket_name, f"dataproduct/sohocredit/unpaidsoho/models/{model_name}.pkl")
    
