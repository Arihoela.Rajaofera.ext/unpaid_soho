from setuptools import setup, find_packages

setup(
    name='unpaid_soho',
    version='0.0.1',
    python_requires=">=3.9",
    packages=find_packages(),
    install_requires=[
        'boto3'
        'pyspark',
        'pandas',
        'scikit-learn'
    ]
)
